const { listOfPosts } = require("./posts");

const getSum = (str1, str2) => {
  if (
    (typeof str1 === "string" || str1 instanceof String) &&
    (typeof str2 === "string" || str2 instanceof String)
  ) {
    let tmp1 = 0;
    let tmp2 = 0;
    if (str1 != "")
     {
      tmp1 = parseInt(str1, 10);
    }
    if (str2 != "")
     {
      tmp2 = parseInt(str2, 10);
    }
    if (
      (tmp1.toString() != str1 && tmp1 != 0) ||
      (tmp2.toString() != str2 && tmp2 != 0)
    ) {
      return false;
    }
    let result = tmp1 + tmp2;
    return result.toString();
  }
  return false;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var myarr = [...listOfPosts];
  let post = 0;
  let com = 0;
  for (let value of myarr) {
    var tmp = { ...value.comments };
    for (let j = 0; j < Object.keys(tmp).length; j++) {
      if (tmp[j].author == authorName) com++;
    }
    if (value.author == authorName) post++;
  }
  return `Post:${post},comments:${com}`;
};

const tickets = (people) => {
  let arr = [...people];
  var a = arr
    .toString()
    .split(",")
    .map(function (item) {
      return parseInt(item, 10);
    });
  if (a[0] != 25 || a[0] != "25") return "NO";
  let tmp = [a[0]];
  for (let i = 1; i < a.length; i++) {
    if (a[i] == 50) {
      let flag = false;
      for (let j = 0; j < tmp.length; j++) {
        if (tmp[j] == 25) {
          flag = true;
          tmp[j] = 50;
          break;
        }
      }
      if (!flag) return "NO";
    } else if (a[i] == 100) {
      let flag = false;
      for (let j = 0; j < tmp.length; j++) {
        if (tmp[j] == 25) {
          for (let u = 0; u < tmp.length; u++) {
            if (tmp[u] == 50) {
              flag = true;
              tmp[u] = 100;
              tmp.splice(j, 1);
              break;
            }
          }
        }
      }
      if (!flag) return "NO";
    } else {
      tmp[tmp.length] = 25;
    }
  }
  return "YES";
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
